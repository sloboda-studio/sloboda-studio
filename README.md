<a href="https://sloboda-studio.com/">Sloboda Studio</a> is a Software Development Company founded in 2010. We’ve been providing high-end web development services in various industries for almost 10 years, mostly focused on leading start-ups, small businesses and middle-market companies.
Was founded 10 years ago, and we have gathered an expertise in the following areas: 
 -Marketplaces 
 -Data Science, Machine Learning, Text Processing 
 -News processing
 -Chatbots development
 -Social media managers 
 -Event Platforms 
Technologies we use: 
 -Backend: Ruby/Ruby on Rails, PHP, Node.js, Python
 -Frontend: JavaScript (React.js, Vue.js, Angular.js) 
 -Mobile development: Java, Swift, Flutter
 -DB: MySQL, PostgreSQL, MongoDB, Redis
 -Testing: Rspec, FactoryGirl, Faker
 